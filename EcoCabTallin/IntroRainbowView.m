//
//  IntroRainbowView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "IntroRainbowView.h"

@interface IntroRainbowView()
{
    UIImageView *rainbow;
    UIView *gradientView;
}

@end

@implementation IntroRainbowView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    self.backgroundColor=[UIColor whiteColor];
    
    float colors[9][3]={
        {5,65,61},
        {54,176,124},
        {236,212,69},
        {54,176,124},
        {5,65,61},
        {54,176,124},
        {236,212,69},
        {54,176,124}
    };
    
    NSMutableArray *rainbowColors=[[NSMutableArray alloc] init];
    for(int i=0; i<8; i++)
    {
        [rainbowColors addObject:(id)[UIColor colorWithRed:colors[i][0]/255 green:colors[i][1]/255 blue:colors[i][2]/255 alpha:1].CGColor];
    }
    
    UIView *bottomView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 69)];
    bottomView.center=self.center;
    bottomView.clipsToBounds=YES;
    [self addSubview:bottomView];
    
    gradientView=[[UIView alloc] initWithFrame:CGRectMake(-bottomView.bounds.size.width*2, 0, bottomView.bounds.size.width*3, bottomView.bounds.size.height*3)];
    CAGradientLayer *gradient=[CAGradientLayer layer];
    gradient.frame=gradientView.bounds;
    gradient.colors=rainbowColors;
    gradient.startPoint=CGPointMake(0, 1);
    gradient.endPoint=CGPointMake(0.5, 0);
    [gradientView.layer addSublayer:gradient];

    [bottomView addSubview:gradientView];
    
    UIImageView *mask=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, bottomView.bounds.size.width+0.1, bottomView.bounds.size.height+0.1)];
    mask.image=[self createMaskView];
    [bottomView addSubview:mask];

    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat animations:^{
        gradientView.center=CGPointMake(gradientView.center.x+bottomView.bounds.size.width*2, gradientView.center.y-(bottomView.bounds.size.height*2));
    } completion:nil];
    
    return self;
}

-(void) stopAnimation
{
    [gradientView.layer removeAllAnimations];
}


-(UIImage *) createMaskView
{
    CGImageRef image=[UIImage imageNamed:@"my_launch_image.png"].CGImage;
    
    CFDataRef dataRef;
    dataRef=CGDataProviderCopyData(CGImageGetDataProvider(image));
    UInt8 *buffer=(UInt8 *)CFDataGetBytePtr(dataRef);

    int width=(int)CGImageGetWidth(image);
    int height=(int)CGImageGetHeight(image);
    int bpr=(int)CGImageGetBytesPerRow(image);
    for(int y=0; y<height; y++)
        for (int x=0; x<width; x++)
        {
            if(buffer[y*bpr+x*4+3]==0x0)
            {
                buffer[y*bpr+x*4]=0xff;
                buffer[y*bpr+x*4+1]=0xff;
                buffer[y*bpr+x*4+2]=0xff;
                buffer[y*bpr+x*4+3]=0xff;
            }
            else
            {
                buffer[y*bpr+x*4]=0x0;
                buffer[y*bpr+x*4+1]=0x0;
                buffer[y*bpr+x*4+2]=0x0;
                buffer[y*bpr+x*4+3]=0x0;
            }
            
        }
    
    CGContextRef context=CGBitmapContextCreate(buffer,
                                width,
                                height,
                                8,
                                bpr,
                                CGImageGetColorSpace(image),
                                kCGImageAlphaPremultipliedLast );
    
    CGImageRef maskImageRef=CGBitmapContextCreateImage(context);
    UIImage *maskImage=[UIImage imageWithCGImage:maskImageRef];
    
    CGContextRelease(context);
    
    return maskImage;
}


@end
