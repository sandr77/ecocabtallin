//
//  ViewController.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    IntroMainView *introMainView;
    CabMapView *mapView;
    SideMenuView *sideMenuView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    mapView=[[CabMapView alloc] initWithFrame:self.view.bounds];
    mapView.delegate=self;
    
    [self.view addSubview:mapView];

    introMainView=[[IntroMainView alloc] initWithFrame:self.view.bounds];
    introMainView.delegate=self;

    [self.view addSubview:introMainView];
    
    sideMenuView=[[SideMenuView alloc] initWithFrame:CGRectMake(-self.view.bounds.size.width*0.72, 0, self.view.bounds.size.width*0.72, self.view.bounds.size.height)];
    [self.view addSubview:sideMenuView];
}

-(void) introductionFinished
{
    [introMainView removeFromSuperview];
    introMainView=nil;
    [mapView mapViewIsGoingToAppear];
}

-(void) cabMapViewUserTappedSideMenu:(CabMapView *)cabMapView
{
    if(sideMenuView.center.x<0)
    {
        [UIView animateWithDuration:0.5 animations:^{
            sideMenuView.center=CGPointMake(sideMenuView.center.x+sideMenuView.bounds.size.width, sideMenuView.center.y);
            mapView.center=CGPointMake(mapView.center.x+sideMenuView.bounds.size.width, mapView.center.y);
        }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            sideMenuView.center=CGPointMake(sideMenuView.center.x-sideMenuView.bounds.size.width, sideMenuView.center.y);
            mapView.center=CGPointMake(mapView.center.x-sideMenuView.bounds.size.width, mapView.center.y);
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
