//
//  ChooseCarTypeView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "ChooseCarTypeView.h"
#import "EcoCabHelper.h"

#define BUTTON_HEIGHT 40
#define BUTTON_WIDTH 100
#define BUTTON_DIST 5

@implementation ChooseCarTypeView

-(id) init
{
    self=[super initWithFrame:[[UIApplication sharedApplication].delegate window].bounds];
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = self.bounds;
    [self addSubview:visualEffectView];
    
    NSArray *types=[EcoCabHelper carTypes];
    CGFloat totalHeight=types.count*BUTTON_HEIGHT+(types.count-1)*BUTTON_DIST;
    CGFloat offset=(self.bounds.size.height-totalHeight)/2;
    
    for(NSString *type in types)
    {
        UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
        button.tag=[types indexOfObject:type];
        button.frame=CGRectMake(30, offset, BUTTON_WIDTH, BUTTON_HEIGHT);
        [button setTitle:type forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:14];
        button.backgroundColor=[UIColor colorWithRed:117.0/255 green:209.0/255 blue:254.0/255 alpha:1];
        if([type isEqualToString:[[EcoCabHelper sharedInstance] currentCarType]])
            button.backgroundColor=[UIColor colorWithRed:59.0/255 green:190.0/255 blue:255.0/255 alpha:1];
        
        button.layer.cornerRadius=BUTTON_HEIGHT/2;
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        offset+=(BUTTON_HEIGHT+BUTTON_DIST);

    }

    
    return self;
}

-(void) buttonPressed:(UIButton *) button
{
    NSArray *types=[EcoCabHelper carTypes];
    [EcoCabHelper sharedInstance].currentCarType=types[button.tag];
    if([self.delegate respondsToSelector:@selector(chooseCarTypeViewChangedCarTo:)])
    {
        [self.delegate chooseCarTypeViewChangedCarTo:types[button.tag]];
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha=0;
    } completion:^(BOOL finished)
     {
         [self removeFromSuperview];
     }];

    
}

-(void) show
{
    self.alpha=0;
    [[[UIApplication sharedApplication].delegate window] addSubview:self];

    [UIView animateWithDuration:0.5 animations:^{
        self.alpha=1;
    }];
}

@end
