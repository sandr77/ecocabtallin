//
//  MapDestinationView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "MapDestinationView.h"


@interface MapDestinationView()
{
    MapAddressView *departureView;
    MapAddressView *destinationView;
    NSTimer *timerToShow;

}

@end

@implementation MapDestinationView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    self.isUserSelectedRoute=NO;
    self.clipsToBounds=NO;
    
    departureView=[[MapAddressView alloc] initWithFrame:CGRectMake(0, frame.size.height/2, frame.size.width, frame.size.height/2)];
    departureView.image=[UIImage imageNamed:@"gray_pointer.png"];
    destinationView=[[MapAddressView alloc] initWithFrame:departureView.bounds];
    destinationView.image=[UIImage imageNamed:@"gray_pointer.png"];
    
    [self addSubview:destinationView];
    [self addSubview:departureView];
    departureView.hidden=YES;
    self.hidden=YES;
    
    destinationView.shouldShowConfirmButton=YES;
    destinationView.delegate=self;
    
    return self;
}

-(void) setDepartureAddress:(NSString *)departureAddress
{
    departureView.addressLabel.text=departureAddress;
}

-(void) setDestinationAddress:(NSString *)destinationAddress
{
    destinationView.addressLabel.text=destinationAddress;
}

-(void) mapAddressViewPressedConfirm:(MapAddressView *)mapAddressView
{
    destinationView.image=[UIImage imageNamed:@"green_sign_mark.png"];
    departureView.alpha=0;
    departureView.hidden=NO;
    self.shouldRespondToShowHide=NO;
    departureView.center=CGPointMake(departureView.center.x, self.bounds.size.height+departureView.bounds.size.height/2);
    [UIView animateWithDuration:0.5 animations:^{
        destinationView.center=CGPointMake(destinationView.center.x, destinationView.bounds.size.height/2);
        departureView.center=CGPointMake(departureView.center.x, destinationView.bounds.size.height+departureView.bounds.size.height/2);
        departureView.alpha=1;
    
    } completion:^(BOOL finished){
        UIView *greenLine=[[UIView alloc] initWithFrame:CGRectMake(19, destinationView.bounds.size.height-7, 2, 14)];
        greenLine.backgroundColor=[UIColor colorWithRed:129.0/255 green:208.0/255 blue:105.0/255 alpha:1];
        [self addSubview:greenLine];
    }];
    if([self.delegate respondsToSelector:@selector(mapDestinationViewConfirmed:)])
        [self.delegate mapDestinationViewConfirmed:self];
    
}

-(void) show
{
    if(!self.shouldRespondToShowHide)
        return;
    [timerToShow invalidate];
    timerToShow=[NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(itsShowTime) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:timerToShow forMode:NSDefaultRunLoopMode];
}

-(void) itsShowTime
{
    if(!self.shouldRespondToShowHide)
        return;
    destinationView.alpha=0;
    destinationView.center=CGPointMake(destinationView.center.x, -destinationView.frame.size.height);
    self.hidden=NO;
    [UIView animateWithDuration:0.5 animations:^{
        destinationView.alpha=1;
        destinationView.center=CGPointMake(destinationView.center.x, destinationView.frame.size.height*1.5);
    }];
}

-(void) hide
{
    if(!self.shouldRespondToShowHide)
        return;
    [timerToShow invalidate];
    
    [UIView animateWithDuration:0.5 animations:^{
        destinationView.alpha=0;
        destinationView.center=CGPointMake(destinationView.center.x, -destinationView.frame.size.height);
    } completion:^(BOOL finished)
     {
         self.hidden=YES;
     }];
    
}



@end
