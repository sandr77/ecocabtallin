//
//  MapAddressView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapAddressView : UIView

@property (strong, nonatomic) UILabel *addressLabel;
@property (strong, nonatomic) UIImage *image;

@property BOOL shouldShowConfirmButton;

@property id delegate;

@end

@protocol MapAddressViewDelegate

-(void) mapAddressViewPressedConfirm:(MapAddressView *) mapAddressView;

@end
