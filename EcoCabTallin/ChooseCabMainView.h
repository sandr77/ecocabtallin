//
//  ChooseCabMainView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseCarTypeView.h"

@interface ChooseCabMainView : UIView <ChooseCarTypeViewDelegate>

-(void) show;

@end
