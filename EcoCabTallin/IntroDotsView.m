//
//  IntroDotsView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "IntroDotsView.h"
#import "IntroCardView.h"
#define DOT_DIAMETER 4.0
#define DOT_INTERVAL 10.0

@interface IntroDotsView()
{
    int dotsCount;
    NSMutableArray *dotViews;
    UIView *currentRaisedDotView;
}

@end

@implementation IntroDotsView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    dotsCount=0;
    self.center=CGPointMake(frame.origin.x+frame.size.width/2, frame.origin.y+frame.size.height/2);
    return self;
}

-(id) initWithCenter:(CGPoint) center numberOfDots:(int) dotsNum
{
    self=[super init];
    dotsCount=dotsNum;
    self.frame=CGRectZero;
    self.center=center;
    [self createDotViews];
    return self;
}

-(id) init
{
    self=[super init];
    dotsCount=0;
    return self;
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if(dotsCount)
    {
        CGPoint center=self.center;
        [super setFrame:CGRectMake(0, 0, dotsCount*DOT_INTERVAL, DOT_INTERVAL)];
        self.center=center;
    }
    
}

-(int) numberOfDots
{
    return dotsCount;
}

-(void) createDotViews
{
    if(!dotsCount)
        return;
    dotViews=[[NSMutableArray alloc] init];
    for(int i=0;i<dotsCount;i++)
    {
        UIView *v=[[UIView alloc] initWithFrame:CGRectMake(0, 0, DOT_DIAMETER, DOT_DIAMETER)];
        v.layer.cornerRadius=DOT_DIAMETER/2;
        v.backgroundColor=[UIColor whiteColor];
        v.center=CGPointMake(DOT_INTERVAL*((float)i-0.5), DOT_INTERVAL-DOT_DIAMETER/2);
        [self addSubview:v];
        [dotViews addObject:v];
    }
}

-(void) raiseDotNumber:(int)num
{
    if(num==dotsCount-1)
    {
        self.hidden=YES;
    }
    else
        self.hidden=NO;
    [UIView animateWithDuration:ANIMATION_DURATION delay:ANIMATION_DURATION options:UIViewAnimationOptionCurveEaseInOut animations:^{
        if(currentRaisedDotView)
            currentRaisedDotView.center=CGPointMake(currentRaisedDotView.center.x, currentRaisedDotView.center.y+(DOT_INTERVAL-DOT_DIAMETER)/2);
        currentRaisedDotView=dotViews[num];
        currentRaisedDotView.center=CGPointMake(currentRaisedDotView.center.x, currentRaisedDotView.center.y-(DOT_INTERVAL-DOT_DIAMETER)/2);
    } completion:nil];
}


@end
