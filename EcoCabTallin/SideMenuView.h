//
//  SideMenuView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuView : UIView

@property id delegate;

@end

@protocol SideMenuViewDelegate



@end