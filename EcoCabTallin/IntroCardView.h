//
//  IntroCardView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//



#import <UIKit/UIKit.h>

#define ANIMATION_DURATION 0.8


typedef enum {IntroCardPositionLeft, IntroCardPositionMiddle, IntroCardPositionRight} IntroCardPosition;

@interface IntroCardView : UIView

@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) UIImage *backgroundImage;
@property (strong, nonatomic) UIImage *emblemImage;

@property int orderNum;
@property BOOL isLastCard;

@property IntroCardPosition position;

@property id delegate;

-(void) slideLeft;
-(void) slideRight;

@end

@protocol IntroCardViewDelegate

-(void) introCardViewPressedPrevious:(IntroCardView *) view;
-(void) introCardViewPressedNext:(IntroCardView *) view;
-(void) introCardViewPressedStart:(IntroCardView *)view;


@end
