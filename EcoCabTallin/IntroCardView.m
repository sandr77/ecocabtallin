//
//  IntroCardView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "IntroCardView.h"

#define ROTATION_ANGLE M_PI/6

@interface IntroCardView()
{
    UILabel *numberLabel;
    UILabel *textLabel;
    UIImageView *emblemView;
    UIImageView *backgroundView;
    IntroCardPosition currentPosition;
    
    int cardOrderNum;
    
    CGPoint originalCenter;
    
    UIButton *previousButton;
    UIButton *nextButton;
    BOOL flagLastCard;
    
    UIButton *startButton;

}
@end

@implementation IntroCardView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    flagLastCard=NO;
    
    backgroundView=[[UIImageView alloc] init];
    backgroundView.backgroundColor=nil;
    backgroundView.opaque=NO;
    [self addSubview:backgroundView];
    
    emblemView=[[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width/3, frame.size.width/4, frame.size.width/3, frame.size.width/3)];
    emblemView.backgroundColor=nil;
    emblemView.opaque=NO;
    [self addSubview:emblemView];

    
    self.layer.cornerRadius=3;
    
    self.layer.shadowColor=[UIColor blackColor].CGColor;
    self.layer.shadowRadius=5;
    self.layer.shadowOpacity=0.7;
    self.layer.shadowOffset=CGSizeMake(5, 5);
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;

    numberLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height/2, frame.size.width/3, frame.size.width/3)];
    numberLabel.backgroundColor=nil;
    numberLabel.opaque=NO;
    numberLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:70];
    numberLabel.textColor=[UIColor whiteColor];
    numberLabel.textAlignment=NSTextAlignmentCenter;
    [self addSubview:numberLabel];
    
    textLabel=[[UILabel alloc] init];
    textLabel.backgroundColor=nil;
    textLabel.opaque=NO;
    textLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:30];
    textLabel.textAlignment=NSTextAlignmentLeft;
    textLabel.textColor=[UIColor whiteColor];
    textLabel.numberOfLines=0;
    [self addSubview:textLabel];
    
    originalCenter=self.center;
    
    previousButton=[self buttonTemplate];
    [previousButton setTitle:@"Previous" forState:UIControlStateNormal];
    previousButton.frame=CGRectMake(20, self.bounds.size.height-50, 100, 25);
    [previousButton addTarget:self action:@selector(previousPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:previousButton];
    
    nextButton=[self buttonTemplate];
    [nextButton setTitle:@"Next" forState:UIControlStateNormal];
    nextButton.frame=CGRectMake(self.bounds.size.width-20-100, self.bounds.size.height-50, 100, 25);
    [nextButton addTarget:self action:@selector(nextPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:nextButton];

    
    return self;
}

-(void) setIsLastCard:(BOOL)isLastCard
{
    flagLastCard=isLastCard;
    if(flagLastCard)
    {
        nextButton.hidden=YES;
        previousButton.hidden=YES;
        startButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [startButton setTitle:@"Let's grab a cab" forState:UIControlStateNormal];
        startButton.backgroundColor=[UIColor whiteColor];
        [startButton setTitleColor:[UIColor colorWithWhite:0.5 alpha:1] forState:UIControlStateNormal];
        startButton.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:18];
        startButton.frame=CGRectMake(0, 0, 170, 48);
        startButton.center=CGPointMake(self.bounds.size.width/2, self.bounds.size.height-60);
        startButton.layer.cornerRadius=24;
        [startButton addTarget:self action:@selector(startButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:startButton];
    }
}

-(void) startButtonPressed
{
    UIGraphicsBeginImageContextWithOptions(startButton.bounds.size, NO, 0.0);
    [startButton.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *buttonImage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *buttonImageView=[[UIImageView alloc] initWithFrame:startButton.frame];
    buttonImageView.image=buttonImage;
    [self addSubview:buttonImageView];
    [startButton removeFromSuperview];
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        CGPoint center=buttonImageView.center;
        buttonImageView.frame=CGRectMake(0, 0, buttonImageView.bounds.size.width*2, buttonImageView.bounds.size.height*2);
        buttonImageView.center=center;
    }];
    if([self.delegate respondsToSelector:@selector(introCardViewPressedStart:)])
        [self.delegate introCardViewPressedStart:self];
}

-(BOOL) isLastCard
{
    return flagLastCard;
}

-(void) previousPressed
{
    if([self.delegate respondsToSelector:@selector(introCardViewPressedPrevious:)])
        [self.delegate introCardViewPressedPrevious:self];
}

-(void) nextPressed
{
    if([self.delegate respondsToSelector:@selector(introCardViewPressedNext:)])
        [self.delegate introCardViewPressedNext:self];
}


-(void) setText:(NSString *)text
{
    textLabel.text=text;

    CGSize size=[textLabel sizeThatFits:CGSizeMake(self.bounds.size.width*0.66, self.bounds.size.height/3)];
    textLabel.frame=CGRectMake(self.bounds.size.width*0.33, numberLabel.center.y-size.height/2, size.width, size.height);
}

-(void) setOrderNum:(int)orderNum
{
    cardOrderNum=orderNum;
    numberLabel.text=[NSString stringWithFormat:@"%d", orderNum];
    if(cardOrderNum==1)
        previousButton.hidden=YES;
}

-(int) orderNum
{
    return cardOrderNum;
}

-(void) setEmblemImage:(UIImage *)emblemImage
{
    emblemView.image=emblemImage;
}

-(void) setBackgroundImage:(UIImage *)backgroundImage
{
    backgroundView.image=backgroundImage;
    
    backgroundView.frame=CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.width*(backgroundImage.size.height/backgroundImage.size.width));
    
}

-(void) setPosition:(IntroCardPosition)position
{
    currentPosition=position;
    if(currentPosition==IntroCardPositionMiddle)
    {
        self.alpha=1;
    }
    else
        self.alpha=0;
    if(currentPosition==IntroCardPositionRight)
    {
        self.center=CGPointMake([UIApplication sharedApplication].delegate.window.bounds.size.width*2, originalCenter.y);
        self.transform=CGAffineTransformMakeRotation(ROTATION_ANGLE);
    }
    else if(currentPosition==IntroCardPositionLeft)
        self.center=CGPointMake(-[UIApplication sharedApplication].delegate.window.bounds.size.width, originalCenter.y);

}

-(IntroCardPosition) position
{
    return currentPosition;
}

-(void) slideLeft
{
    if(currentPosition==IntroCardPositionMiddle)
    {
        [UIView animateWithDuration:ANIMATION_DURATION delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.center=CGPointMake(-[UIApplication sharedApplication].delegate.window.bounds.size.width, originalCenter.y);
            self.alpha=0;
            self.transform=CGAffineTransformMakeRotation(-ROTATION_ANGLE);

        } completion:^(BOOL finished){
            
            currentPosition=IntroCardPositionLeft;
        }];
    }
    else if (currentPosition==IntroCardPositionRight)
    {
        [UIView animateWithDuration:ANIMATION_DURATION delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.center=originalCenter;
            self.alpha=1;
            self.transform=CGAffineTransformMakeRotation(0);

        } completion:^(BOOL finished){
            currentPosition=IntroCardPositionMiddle;
        }];

    }
}

-(void) slideRight
{

    if(currentPosition==IntroCardPositionMiddle)
    {
        [UIView animateWithDuration:ANIMATION_DURATION delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.center=CGPointMake([UIApplication sharedApplication].delegate.window.bounds.size.width*2, originalCenter.y);
            self.alpha=0;
            self.transform=CGAffineTransformMakeRotation(ROTATION_ANGLE);

        } completion:^(BOOL finished){
            
            currentPosition=IntroCardPositionRight;
        }];
    }
    else if (currentPosition==IntroCardPositionLeft)
    {
        [UIView animateWithDuration:ANIMATION_DURATION delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.center=originalCenter;
            self.alpha=1;
            self.transform=CGAffineTransformMakeRotation(0);

        } completion:^(BOOL finished){
            currentPosition=IntroCardPositionMiddle;
        }];
    }
}

-(UIButton *) buttonTemplate
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    [button setTitleColor:[UIColor colorWithWhite:0.8 alpha:1] forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:18];
    return button;
}




@end
