//
//  IntroDotsView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroDotsView : UIView

-(id) initWithCenter:(CGPoint) center numberOfDots:(int) dotsNum;

-(void) raiseDotNumber:(int) num;

@end
