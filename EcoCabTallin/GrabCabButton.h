//
//  GrabCabButton.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GrabCabButton : UIView

@property int distance;

@end
