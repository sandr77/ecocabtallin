//
//  SingleCabView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "SingleCabView.h"

@interface SingleCabView()
{
    UIImageView *carView;
    UILabel *startPriceLabel;
    UILabel *priceKmLabel;
    UILabel *priceHrLabel;
    UILabel *seatsLabel;
    UILabel *carModelLabel;
    UILabel *driverNameLabel;
    
    NSDictionary *carInfoDict;
}

@end

@implementation SingleCabView

-(id) initWithFrame:(CGRect)frame cabInfoDict:(NSDictionary *) dict
{
    self=[super initWithFrame:frame];
    carInfoDict=dict;
    
    carView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, frame.size.height-20, frame.size.height-20)];
    carView.layer.cornerRadius=carView.bounds.size.height/2;
    carView.clipsToBounds=YES;
    carView.image=dict[@"carImage"];
    [self addSubview:carView];
    
    CGFloat dist=self.bounds.size.height/5;
    CGFloat offset=dist;
    
    startPriceLabel=[self labelTemplateWithFontSize:16];
    startPriceLabel.text=carInfoDict[@"priceStart"];
    startPriceLabel.center=CGPointMake(startPriceLabel.center.x, offset);
    
    offset+=dist;
    priceKmLabel=[self labelTemplateWithFontSize:16];
    priceKmLabel.text=carInfoDict[@"priceKm"];
    priceKmLabel.center=CGPointMake(priceKmLabel.center.x, offset);

    priceHrLabel=[self labelTemplateWithFontSize:12];
    priceHrLabel.text=carInfoDict[@"priceHr"];
    priceHrLabel.textColor=[UIColor lightGrayColor];
    priceHrLabel.textAlignment=NSTextAlignmentRight;
    priceHrLabel.center=CGPointMake(priceKmLabel.center.x, offset);

    
    
    offset+=dist;
    carModelLabel=[self labelTemplateWithFontSize:14];
    carModelLabel.textColor=[UIColor lightGrayColor];
    carModelLabel.text=carInfoDict[@"carModel"];
    carModelLabel.center=CGPointMake(carModelLabel.center.x, offset);
    
    seatsLabel=[self labelTemplateWithFontSize:12];
    seatsLabel.text=carInfoDict[@"seats"];
    seatsLabel.textColor=[UIColor lightGrayColor];
    seatsLabel.textAlignment=NSTextAlignmentRight;
    seatsLabel.center=CGPointMake(priceKmLabel.center.x, offset);

    
    offset+=dist;
    driverNameLabel=[self labelTemplateWithFontSize:14];
    driverNameLabel.textColor=[UIColor lightGrayColor];
    driverNameLabel.text=carInfoDict[@"driverName"];
    driverNameLabel.center=CGPointMake(driverNameLabel.center.x, offset);

    
    
    
    return self;
}

-(UILabel *) labelTemplateWithFontSize:(CGFloat) fontSize
{
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.height, 0, self.bounds.size.width-self.bounds.size.height-10, 20)];
    label.textColor=[UIColor blackColor];
    label.font=[UIFont fontWithName:@"Roboto-Regular" size:fontSize];
    label.textAlignment=NSTextAlignmentLeft;
    [self addSubview:label];
    return label;
}

@end
