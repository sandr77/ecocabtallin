//
//  IntroMainView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "IntroMainView.h"
#import "IntroDotsView.h"
#import "IntroRainbowView.h"

#define RAINBOW_DELAY 3

@interface IntroMainView()
{
    NSMutableArray *introCards;
    NSArray *cardsData;
    IntroDotsView *dotsView;
    UIImageView *lightningView;
    IntroRainbowView *rainbowView;
}

@end

@implementation IntroMainView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    self.backgroundColor=[UIColor whiteColor];
    UIImage *lightning=[UIImage imageNamed:@"lightning.png"];
    lightningView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, (lightning.size.height/lightning.size.width)*20)];
    lightningView.image=lightning;
    
    
    cardsData=@[@{@"emblem":@"car1.png", @"background":@"leaves.png", @"text":@"Most\neconomy\nfriendly taxi\nservice.", @"backColor":[UIColor colorWithRed:0 green:151.0/255 blue:226.0/255 alpha:1], @"cardColor":[UIColor colorWithRed:59.0/255 green:190.0/255 blue:1 alpha:1]},
                @{@"emblem":@"piggie.png", @"background":@"euros.png", @"text":@"Therefore\na lot cheaper\nthan average\ntaxi ride.", @"backColor":[UIColor colorWithRed:107.0/255 green:184.0/255 blue:82.0/255 alpha:1], @"cardColor":[UIColor colorWithRed:29.0/255 green:148.0/255 blue:68.0/255 alpha:1]},
                @{@"emblem":@"car2.png", @"background":@"smiles.png", @"text":@"As simple as\nthat. Happy\ntaxyriding.", @"backColor":[UIColor colorWithRed:234.0/255 green:201.0/255 blue:0.0/255 alpha:1], @"cardColor":[UIColor colorWithRed:234.0/255 green:216.0/255 blue:0 alpha:1]}
                ];
    
    introCards=[[NSMutableArray alloc] init];
    
    for(NSDictionary *dict in cardsData)
    {
        IntroCardView *card=[[IntroCardView alloc] initWithFrame:CGRectMake(10, frame.size.height/6+5, frame.size.width-20, frame.size.height*(5.0/6))];
        card.text=dict[@"text"];
        card.orderNum=(int)[cardsData indexOfObject:dict]+1;
        card.emblemImage=[UIImage imageNamed:dict[@"emblem"]];
        card.backgroundImage=[UIImage imageNamed:dict[@"background"]];
        card.backgroundColor=dict[@"cardColor"];
        card.delegate=self;
        card.position=IntroCardPositionRight;

        [introCards addObject:card];
        [self addSubview:card];
    }
    
    ((IntroCardView *)introCards.lastObject).isLastCard=YES;
    
    dotsView=[[IntroDotsView alloc] initWithCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height-40) numberOfDots:(int)[introCards count]];
    [self addSubview:dotsView];
    dotsView.alpha=0;
    

    rainbowView=[[IntroRainbowView alloc] initWithFrame:self.bounds];
    [self addSubview:rainbowView];
    
    self.backgroundColor=cardsData[0][@"backColor"];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(RAINBOW_DELAY * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            rainbowView.alpha=0;
        
        } completion:^(BOOL finished){
            [rainbowView stopAnimation];
            [rainbowView removeFromSuperview];
            rainbowView=nil;
            [self showFirstCard];            
        }];
    });

    
    
    return self;
}


-(void) showFirstCard
{
    IntroCardView *card=introCards[0];
    
    [card slideLeft];
    [UIView animateWithDuration:ANIMATION_DURATION delay:ANIMATION_DURATION options:UIViewAnimationOptionCurveEaseOut animations:^{
        dotsView.alpha=1;
    } completion:nil];

    [dotsView raiseDotNumber:0];
    
    lightningView.center=CGPointMake(self.bounds.size.width/2, card.frame.origin.y+40);
    lightningView.alpha=0;
    [self insertSubview:lightningView atIndex:0];
    [UIView animateWithDuration:ANIMATION_DURATION delay:ANIMATION_DURATION/2 options:UIViewAnimationOptionCurveEaseOut animations:^{
        lightningView.center=CGPointMake(lightningView.center.x, card.frame.origin.y/2);
        lightningView.alpha=1;
    } completion:nil];

}

-(void) introCardViewPressedStart:(IntroCardView *)view
{
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        self.alpha=0;
    } completion:^(BOOL finished){
        if([self.delegate respondsToSelector:@selector(introductionFinished)])
            [self.delegate introductionFinished];
        else
            [self removeFromSuperview];
    }];
}

-(void) introCardViewPressedPrevious:(IntroCardView *)view
{
    [view slideRight];
    int num=view.orderNum-1;
    num--;
    [dotsView raiseDotNumber:num];
    IntroCardView *prevView=introCards[num];
    [prevView slideRight];
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        self.backgroundColor=cardsData[num][@"backColor"];
    }];
}

-(void) introCardViewPressedNext:(IntroCardView *)view
{
    [view slideLeft];
    int num=view.orderNum-1;
    num++;
    [dotsView raiseDotNumber:num];
    IntroCardView *nextView=introCards[num];
    [nextView slideLeft];
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        self.backgroundColor=cardsData[num][@"backColor"];
    }];
}


@end
