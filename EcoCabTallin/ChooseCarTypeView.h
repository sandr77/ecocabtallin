//
//  ChooseCarTypeView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseCarTypeView : UIView

@property id delegate;

-(void) show;

@end

@protocol ChooseCarTypeViewDelegate

-(void) chooseCarTypeViewChangedCarTo:(NSString *) newCarType;

@end
