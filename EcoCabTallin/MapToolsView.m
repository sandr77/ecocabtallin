//
//  MapToolsView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "MapToolsView.h"

@interface MapToolsView()
{
    UIButton *sideMenuButton;
    UIButton *currentLocationButton;
    UIButton *timeButton;
    
    CGPoint centerShown;
    
    NSTimer *timerToShow;
}

@end

@implementation MapToolsView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    self.shouldRespondToShowHide=NO;

    
    sideMenuButton=[self buttonTemplate];
    [sideMenuButton setBackgroundImage:[UIImage imageNamed:@"side_menu_icon.png"] forState:UIControlStateNormal];
    sideMenuButton.center=CGPointMake(sideMenuButton.bounds.size.width/2, self.bounds.size.height/2);
    [sideMenuButton addTarget:self action:@selector(sideMenuPressed) forControlEvents:UIControlEventTouchUpInside];
    
    currentLocationButton=[self buttonTemplate];
    [currentLocationButton setBackgroundImage:[UIImage imageNamed:@"current_location_icon.png"] forState:UIControlStateNormal];
    currentLocationButton.center=CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    [currentLocationButton addTarget:self action:@selector(currentLocationPressed) forControlEvents:UIControlEventTouchUpInside];

    timeButton=[self buttonTemplate];
    [timeButton setBackgroundImage:[UIImage imageNamed:@"time_icon.png"] forState:UIControlStateNormal];
    timeButton.center=CGPointMake(self.bounds.size.width-currentLocationButton.bounds.size.width/2, self.bounds.size.height/2);


    centerShown=self.center;
    self.center=CGPointMake(centerShown.x, -self.bounds.size.height/2);

    return self;
}

-(void) show
{
    if(!self.shouldRespondToShowHide)
        return;
    [timerToShow invalidate];
    timerToShow=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(itsShowTime) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:timerToShow forMode:NSDefaultRunLoopMode];
}

-(void) itsShowTime
{
    if(!self.shouldRespondToShowHide)
        return;
    self.alpha=0;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha=1;
        self.center=centerShown;
    }];
}

-(void) hide
{
    if(!self.shouldRespondToShowHide)
        return;
    [timerToShow invalidate];

    [UIView animateWithDuration:0.5 animations:^{
        self.alpha=0;
        self.center=CGPointMake(centerShown.x, -self.bounds.size.height/2);
    }];

}

-(void) sideMenuPressed
{
    if([self.delegate respondsToSelector:@selector(maptoolsViewUserTappedSideMenu:)])
        [self.delegate maptoolsViewUserTappedSideMenu:self];
}

-(void) currentLocationPressed
{
    
}


-(UIButton *) buttonTemplate
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.frame=CGRectMake(0, 0, 34, 34);
    [self addSubview:button];
    
    return button;
}

-(void) dealloc
{
    [timerToShow invalidate];
}

@end
