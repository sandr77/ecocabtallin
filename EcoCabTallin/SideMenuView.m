//
//  SideMenuView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "SideMenuView.h"

#define BUTTON_VERT_DIST 60
#define USER_NAME @"John Smith"

@interface SideMenuView()
{
    UIButton *historyButton;
    UIButton *paymentsButton;
    UIButton *profileButton;
    UIButton *aboutButton;
    
    UIButton *tourButton;
    UIButton *helpButton;
    
    UIImageView *lightning;
    
    UILabel *usernameLabel;
    UIButton *settingsButton;
}

@end

@implementation SideMenuView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    self.backgroundColor=[UIColor colorWithRed:28.0/255 green:33.0/255 blue:39.0/255 alpha:1];
    
    CGFloat offset=BUTTON_VERT_DIST;
    historyButton=[self buttonTemplateWithTitle:@"History"];
    historyButton.center=CGPointMake(self.bounds.size.width/2, offset);
    
    offset+=BUTTON_VERT_DIST;
    paymentsButton=[self buttonTemplateWithTitle:@"Payments"];
    paymentsButton.center=CGPointMake(self.bounds.size.width/2, offset);

    offset+=BUTTON_VERT_DIST;
    profileButton=[self buttonTemplateWithTitle:@"Profile"];
    profileButton.center=CGPointMake(self.bounds.size.width/2, offset);

    offset+=BUTTON_VERT_DIST;
    aboutButton=[self buttonTemplateWithTitle:@"About"];
    aboutButton.center=CGPointMake(self.bounds.size.width/2, offset);
    
    offset+=BUTTON_VERT_DIST*1.3;
    tourButton=[self buttonTemplateWithTitle:@"Tour"];
    tourButton.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:18];
    [tourButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    tourButton.center=CGPointMake(self.bounds.size.width/2, offset);

    offset+=BUTTON_VERT_DIST*0.8;
    helpButton=[self buttonTemplateWithTitle:@"Help"];
    helpButton.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:18];
    [helpButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    helpButton.center=CGPointMake(self.bounds.size.width/2, offset);


    lightning=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 19)];
    lightning.center=CGPointMake(self.bounds.size.width/12, self.bounds.size.height-30);
    lightning.image=[UIImage imageNamed:@"lightning.png"];
    [self addSubview:lightning];

    usernameLabel=[[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width/6, self.bounds.size.height-42, self.bounds.size.width*0.6, 25)];
    usernameLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:16];
    usernameLabel.textColor=[UIColor lightGrayColor];
    usernameLabel.text=USER_NAME;
    [self addSubview:usernameLabel];
    
    settingsButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [settingsButton setBackgroundImage:[UIImage imageNamed:@"settings_icon.png"] forState:UIControlStateNormal];
    settingsButton.frame=CGRectMake(self.bounds.size.width*0.80, self.bounds.size.height-40, 18, 18);
    [self addSubview:settingsButton];
    
    return self;
}

-(UIButton *) buttonTemplateWithTitle:(NSString *) title
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame=CGRectMake(self.bounds.size.width/6, 0, self.bounds.size.width*(4.0/6), 30);
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:22];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self addSubview:button];
    
    return button;
}

@end
