//
//  ChooseCabMainView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "ChooseCabMainView.h"
#import "SingleCabView.h"
#import "EcoCabHelper.h"
#import "GrabCabButton.h"


@interface ChooseCabMainView()
{
    UIView *containerView;
    NSArray *cabsArray;
    NSDictionary *currentCabDict;
    SingleCabView *currentCabView;
    CGRect cabViewFrame;
    GrabCabButton *grabButton;
    CGPoint originalCenter;
    UIButton *allCarsButton;
    UIButton *refreshButton;
}

@end

@implementation ChooseCabMainView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    originalCenter=self.center;
    
    cabsArray=[EcoCabHelper availableCars];
    
    containerView=[[UIView alloc] initWithFrame:CGRectMake(10, 20, frame.size.width-20, frame.size.height-10)];
    containerView.backgroundColor=[UIColor whiteColor];
    containerView.layer.cornerRadius=5;
    containerView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    containerView.layer.borderWidth=1;
    containerView.clipsToBounds=YES;
    [self addSubview:containerView];
    
    
    self.layer.shadowColor=[UIColor blackColor].CGColor;
    self.layer.shadowOffset=CGSizeMake(5, 5);
    self.layer.shadowOpacity=0.3;


    currentCabDict=cabsArray[0];

    cabViewFrame=CGRectMake(0, 22, containerView.bounds.size.width, containerView.bounds.size.height-90);
    currentCabView=[[SingleCabView alloc] initWithFrame:cabViewFrame cabInfoDict:currentCabDict];
    [containerView addSubview:currentCabView];

    UISwipeGestureRecognizer *swipeLeftGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
    [swipeLeftGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [containerView addGestureRecognizer:swipeLeftGesture];
    UISwipeGestureRecognizer *swipeRightGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
    [swipeRightGesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [containerView addGestureRecognizer:swipeRightGesture];
    
    grabButton=[[GrabCabButton alloc] initWithFrame:CGRectMake(0, containerView.bounds.size.height-60, containerView.bounds.size.width, 60)];
    grabButton.distance=251;
    [containerView addSubview:grabButton];
    
    allCarsButton=[UIButton buttonWithType:UIButtonTypeCustom];
    allCarsButton.frame=CGRectMake(20, 5, 100, 30);
    [allCarsButton setTitle:[EcoCabHelper sharedInstance].currentCarType forState:UIControlStateNormal];
    [allCarsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    allCarsButton.titleLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:12];
    allCarsButton.backgroundColor=[UIColor colorWithRed:66.0/255 green:191.0/255 blue:252.0/255 alpha:1];
    allCarsButton.layer.cornerRadius=15;
    [allCarsButton addTarget:self action:@selector(chooseCar) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:allCarsButton];
    
    refreshButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [refreshButton setBackgroundImage:[UIImage imageNamed:@"refresh_icon.png"] forState:UIControlStateNormal];
    refreshButton.frame=CGRectMake(self.bounds.size.width-54, 3, 34, 34);
    [self addSubview:refreshButton];
    
    self.center=CGPointMake(originalCenter.x, originalCenter.y+self.bounds.size.height);
    
    return self;
}

-(void) chooseCar
{
    ChooseCarTypeView *view=[[ChooseCarTypeView alloc] init];
    view.delegate=self;
    [view show];
}

-(void) chooseCarTypeViewChangedCarTo:(NSString *)newCarType
{
    [allCarsButton setTitle:newCarType forState:UIControlStateNormal];
}

-(void) show
{
    [UIView animateWithDuration:0.5 animations:^{
    
        self.center=originalCenter;
    }];
}

-(void) hide
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.center=CGPointMake(originalCenter.x, originalCenter.y+self.bounds.size.height);
    }];
}

-(void) swipeDetected:(UISwipeGestureRecognizer *) gesture
{
    int direction=1;
    NSInteger index=[cabsArray indexOfObject:currentCabDict];
    if(gesture.direction==UISwipeGestureRecognizerDirectionLeft)
        index++;
    else if(gesture.direction==UISwipeGestureRecognizerDirectionRight)
    {
        direction=-1;
        index--;
    }
    if(index<0)
        index=[cabsArray count]-1;
    if(index>=[cabsArray count])
        index=0;
    currentCabDict=cabsArray[index];
    
    SingleCabView *nextCab=[[SingleCabView alloc] initWithFrame:cabViewFrame cabInfoDict:currentCabDict];
    nextCab.center=CGPointMake(nextCab.center.x+direction*cabViewFrame.size.width, nextCab.center.y);
    [containerView addSubview:nextCab];
    
    [UIView animateWithDuration:0.5 animations:^{
        currentCabView.center=CGPointMake(currentCabView.center.x-direction*cabViewFrame.size.width, currentCabView.center.y);
        nextCab.center=CGPointMake(nextCab.center.x-direction*cabViewFrame.size.width, nextCab.center.y);
    } completion:^(BOOL finished){
        [currentCabView removeFromSuperview];
        currentCabView=nextCab;
    }];
    
}



@end
