//
//  MapView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "CabMapView.h"



@interface CabMapView()
{
    MKMapView *mapView;
    CLLocationManager *locationManager;
    MapToolsView *mapToolsView;
    MapDestinationView *destinationView;
    ChooseCabMainView *chooseCabMainView;
    UIImageView *currentPositionView;
}

@end

@implementation CabMapView
-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    locationManager=[[CLLocationManager alloc] init];
    locationManager.delegate=self;

    
    mapView=[[MKMapView alloc] initWithFrame:self.bounds];
    mapView.delegate=self;
    mapView.mapType=MKMapTypeStandard;
    mapView.rotateEnabled=NO;
    [mapView setCenterCoordinate:mapView.userLocation.location.coordinate animated:NO];
    [self addSubview:mapView];
    
    currentPositionView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 72, 72)];
    currentPositionView.image=[UIImage imageNamed:@"map_pin.png"];
    currentPositionView.center=self.center;
    [self addSubview:currentPositionView];
    
    mapToolsView=[[MapToolsView alloc] initWithFrame:CGRectMake(self.bounds.size.width/10, 30, self.bounds.size.width*0.8, 34)];
    mapToolsView.delegate=self;
    [self addSubview:mapToolsView];
    
    destinationView=[[MapDestinationView alloc] initWithFrame:CGRectMake(20, self.bounds.size.height/4, self.bounds.size.width-40, 90)];
    destinationView.departureAddress=@"Telliskivi 57";
    destinationView.destinationAddress=@"Kaera 22";
    destinationView.delegate=self;
    [self addSubview:destinationView];
    
    chooseCabMainView=[[ChooseCabMainView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height-200, self.bounds.size.width, 200)];
    [self addSubview:chooseCabMainView];
    

    
    return self;
}

-(void) mapViewIsGoingToAppear
{
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
        authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        [locationManager startUpdatingLocation];
        mapView.showsUserLocation = YES;
        
    }
    else
    {
        [locationManager requestWhenInUseAuthorization];
    }
    mapToolsView.shouldRespondToShowHide=YES;
    destinationView.shouldRespondToShowHide=YES;

    
    [mapToolsView show];
    
    [destinationView show];
}

-(void) maptoolsViewUserTappedSideMenu:(MapToolsView *)mapToolsView
{
    if([self.delegate respondsToSelector:@selector(cabMapViewUserTappedSideMenu:)])
        [self.delegate cabMapViewUserTappedSideMenu:self];
}

-(void) mapDestinationViewConfirmed:(MapDestinationView *)view
{
    [chooseCabMainView show];
}

//- (MKAnnotationView *)mapView:(MKMapView *)_mapView viewForAnnotation:(id <MKAnnotation>)annotation
//{
//    static NSString *AnnotationViewID = @"annotationViewID";
//    
//    MKAnnotationView *annotationView = (MKAnnotationView *)[_mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
//    
//    if (annotationView == nil)
//    {
//        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
//    }
//    
//    annotationView.image = [UIImage imageNamed:@"map_pin.png"];//add any image which you want to show on map instead of red pins
//    annotationView.annotation = annotation;
//    
//    return annotationView;
//}

-(void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        [locationManager startUpdatingLocation];
        mapView.showsUserLocation = YES;
        
    }
}

-(void) mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    [mapToolsView hide];
    [destinationView hide];
}

-(void) mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{

    [mapToolsView show];
    [destinationView show];
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    CLLocationCoordinate2D loc = [userLocation.location coordinate];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 1000.0f, 1000.0f);
    [aMapView setRegion:region animated:YES];
}






@end
