//
//  IntroMainView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroCardView.h"


@interface IntroMainView : UIView <IntroCardViewDelegate>

@property id delegate;

@end

@protocol IntroMainViewDelegate

-(void) introductionFinished;

@end
