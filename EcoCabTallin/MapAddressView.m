//
//  MapAddressView.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "MapAddressView.h"

#define TOP_OFFSET 7

@interface MapAddressView()
{
    UIImageView *imageView;
    BOOL flagConfirmButton;
    UIButton *confirmButton;
}

@end

@implementation MapAddressView

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    flagConfirmButton=NO;
    self.backgroundColor=[UIColor whiteColor];
    self.layer.cornerRadius=5;
    self.layer.borderWidth=1;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.layer.shadowColor=[UIColor blackColor].CGColor;
    self.layer.shadowOffset=CGSizeMake(5, 5);
    self.layer.shadowRadius=5;
    self.layer.shadowOpacity=0.3;
    
    imageView=[[UIImageView alloc] initWithFrame:CGRectMake(TOP_OFFSET, TOP_OFFSET, frame.size.height-TOP_OFFSET*2, frame.size.height-TOP_OFFSET*2)];
    [self addSubview:imageView];
    
    self.addressLabel=[[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.origin.x+imageView.frame.size.width+10, TOP_OFFSET, frame.size.width-(imageView.frame.origin.x+imageView.frame.size.width+10), frame.size.height-TOP_OFFSET*2)];
    self.addressLabel.textColor=[UIColor grayColor];
    self.addressLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:16];
    [self addSubview:self.addressLabel];
    
    confirmButton=[UIButton buttonWithType:UIButtonTypeCustom];
    confirmButton.frame=CGRectMake(frame.size.width-frame.size.height, 0, frame.size.height, frame.size.height);
    [confirmButton setBackgroundImage:[UIImage imageNamed:@"confirm_icon.png"] forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(confirmPressed) forControlEvents:UIControlEventTouchUpInside];
    confirmButton.alpha=0;
    [self addSubview:confirmButton];
    
    return self;
}

-(void) confirmPressed
{
    self.shouldShowConfirmButton=NO;
    if([self.delegate respondsToSelector:@selector(mapAddressViewPressedConfirm:)])
        [self.delegate mapAddressViewPressedConfirm:self];
}

-(void) setShouldShowConfirmButton:(BOOL)shouldShowConfirmButton
{
    if(flagConfirmButton==shouldShowConfirmButton)
        return;
    if(shouldShowConfirmButton==NO && flagConfirmButton==YES)
    {
        [UIView animateWithDuration:0.5 animations:^{
            confirmButton.alpha=0;
        }];
    }
    else
    {
            confirmButton.alpha=1;
    }
    flagConfirmButton=shouldShowConfirmButton;
}

-(BOOL) shouldShowConfirmButton
{
    return flagConfirmButton;
}

-(void) setImage:(UIImage *)image
{
    imageView.image=image;
}


@end
