//
//  EcoCabHelper.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "EcoCabHelper.h"
#import <UIKit/UIKit.h>

@interface EcoCabHelper()
{
    NSString *carType;
}

@end

@implementation EcoCabHelper

-(id) init
{
    self=[super init];
    
    carType=[[EcoCabHelper carTypes] lastObject];
    
    return self;
}

-(void) setCurrentCarType:(NSString *)currentCarType
{
    carType=currentCarType;
}

-(NSString *) currentCarType
{
    return carType;
}

+(EcoCabHelper *)sharedInstance {
    static dispatch_once_t once;
    static EcoCabHelper *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+(NSArray *) availableCars
{
    return @[@{
                 @"carImage":[UIImage imageNamed:@"car_01.png"],
                 @"carModel":@"Mercedes G500, 2013",
                 @"seats":@"4 seats",
                 @"priceKm":@"1.50€/km",
                 @"priceStart":@"3.0€ Start",
                 @"priceHr":@"10.00€/h",
                 @"driverName":@"Alexander"
                 },@{
                 @"carImage":[UIImage imageNamed:@"car_02.png"],
                 @"carModel":@"Ford Mustang, 2010",
                 @"seats":@"4 seats",
                 @"priceKm":@"0.50€/km",
                 @"priceStart":@"2.5€ Start",
                 @"priceHr":@"8.00€/h",
                 @"driverName":@"Dmitry"
                 },@{
                 @"carImage":[UIImage imageNamed:@"car_03.png"],
                 @"carModel":@"Lada 2101, 1970",
                 @"seats":@"4 seats",
                 @"priceKm":@"0.1€/km",
                 @"priceStart":@"0.5€ Start",
                 @"priceHr":@"1.00€/h",
                 @"driverName":@"Agafon"
                 }];
}


+(NSArray *) carTypes
{
    return @[@"LPG",
             @"HYBRID",
             @"ELECTRIC",
             @"ALL CARS"
             ];
}

@end
