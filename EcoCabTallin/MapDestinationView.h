//
//  MapDestinationView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapAddressView.h"

@interface MapDestinationView : UIView <MapAddressViewDelegate>

@property (strong, nonatomic) NSString *departureAddress;
@property (strong, nonatomic) NSString *destinationAddress;

@property BOOL isUserSelectedRoute;
@property BOOL shouldRespondToShowHide;

@property id delegate;

-(void) show;
-(void) hide;


@end

@protocol MapDestinationViewDelegate

-(void) mapDestinationViewConfirmed:(MapDestinationView *) view;

@end
