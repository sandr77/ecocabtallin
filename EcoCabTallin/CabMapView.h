//
//  MapView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapToolsView.h"
#import "MapDestinationView.h"
#import "ChooseCabMainView.h"

@import MapKit;

@interface CabMapView : UIView <MKMapViewDelegate, CLLocationManagerDelegate, MapToolsViewDelegate, MapDestinationViewDelegate>

-(void) mapViewIsGoingToAppear;
@property id delegate;

@end


@protocol CabMapViewDelegate

-(void) cabMapViewUserTappedSideMenu:(CabMapView *) cabMapView;

@end