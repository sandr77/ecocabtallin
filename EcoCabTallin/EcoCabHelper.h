//
//  EcoCabHelper.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EcoCabHelper : NSObject

@property (strong, nonatomic) NSString *currentCarType;

+(EcoCabHelper *) sharedInstance;
+(NSArray *) availableCars;

+(NSArray *) carTypes;


@end
