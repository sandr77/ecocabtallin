//
//  MapToolsView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapToolsView : UIView


@property BOOL shouldRespondToShowHide;
@property id delegate;
-(void) show;
-(void) hide;




@end


@protocol MapToolsViewDelegate

-(void) maptoolsViewUserTappedSideMenu:(MapToolsView *) mapToolsView;

@end