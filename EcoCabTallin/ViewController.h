//
//  ViewController.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroMainView.h"
#import "SideMenuView.h"
#import "CabMapView.h"



@interface ViewController : UIViewController <IntroMainViewDelegate, CabMapViewDelegate>


@end

