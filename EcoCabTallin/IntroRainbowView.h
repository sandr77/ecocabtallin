//
//  IntroRainbowView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 23/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroRainbowView : UIView

-(void) stopAnimation;

@end
