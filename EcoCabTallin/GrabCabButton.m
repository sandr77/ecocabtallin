//
//  GrabCabButton.m
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import "GrabCabButton.h"

@interface GrabCabButton()
{
    int distanceToDestination;
    UILabel *distanceLabel;
}

@end

@implementation GrabCabButton

-(id) initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    UIView *v1=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height+5)];
    v1.layer.cornerRadius=5;
    v1.backgroundColor=[UIColor colorWithRed:36.0/255 green:147.0/255 blue:71.0/255 alpha:1];
    [self addSubview:v1];
    
    distanceLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 5, 50, self.bounds.size.height-20)];
    distanceLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:14];
    distanceLabel.textAlignment=NSTextAlignmentCenter;
    distanceLabel.textColor=[UIColor colorWithRed:109.0/255 green:183.0/255 blue:87.0/255 alpha:1];
    [v1 addSubview:distanceLabel];
    
    UIView *v2=[[UIView alloc] initWithFrame:CGRectMake(70, 0, self.bounds.size.width-70, self.bounds.size.height+5)];
    v2.layer.cornerRadius=5;
    v2.backgroundColor=[UIColor colorWithRed:109.0/255 green:183.0/255 blue:87.0/255 alpha:1];
    [self addSubview:v2];
    
    UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedButton)];
    [v2 addGestureRecognizer:gesture];
    
    UILabel *grabLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, v2.bounds.size.width-40, v2.bounds.size.height-20)];
    grabLabel.textColor=[UIColor whiteColor];
    grabLabel.font=[UIFont fontWithName:@"Roboto-Regular" size:22];
    grabLabel.text=@"Grab a cab";
    grabLabel.textAlignment=NSTextAlignmentCenter;
    [v2 addSubview:grabLabel];
    
    UIImageView *greenLightning=[[UIImageView alloc] initWithFrame:CGRectMake(grabLabel.frame.origin.x+grabLabel.bounds.size.width-5, self.bounds.size.height/2-12-5, 24, 24)];
    greenLightning.image=[UIImage imageNamed:@"green_lightning.png"];
    [v2 addSubview:greenLightning];
    
    return self;
}

-(void) userTappedButton
{
    
}

-(void) setDistance:(int)distance
{
    distanceLabel.text=[NSString stringWithFormat:@"%dm", distance];
    distanceToDestination=distance;
}

-(int) distance
{
    return distanceToDestination;
}


@end
