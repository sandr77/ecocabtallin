//
//  SingleCabView.h
//  EcoCabTallin
//
//  Created by Andrey Snetkov on 24/12/15.
//  Copyright © 2015 sandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleCabView : UIView
-(id) initWithFrame:(CGRect)frame cabInfoDict:(NSDictionary *) dict;
@end
